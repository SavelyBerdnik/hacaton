import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton


class Window(QMainWindow):
    def __init__(self):

        super().__init__()
        self.initUI()

    def initUI(self):

        self.setGeometry(300, 300, 300, 300)
        self.setWindowTitle('Главная форма')
        self.btn = QPushButton('Другая форма', self)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Window()
    ex.show()
    sys.exit(app.exec())
